# CopyrightMark

CopyrightMark是一个前端网页内容版权标记插件。当在网页中使用该插件，用户在浏览网页时如果复制网页中的内容就会自动在复制的内容中附上一段版权信息，提醒用户注意版权。 

## 支持浏览器

支持 IE8+ 及所有通用的浏览器

## 使用方法

```javascript
copyrightmark({options});
```

### 示例 #1

```
<script src="copyrightmark.min.js"></script>
<script>
copyrightmark({
    selector: 'article',
    minCopy: 120,
    //...
});
</script>
```

## License

copyrightmark.js is available under the [MIT license].

[MIT License]:http://opensource.org/licenses/MIT


## 个人官方网址

https://www.fuload.cn/copyrightmark

官方网址有更详细的手册及示例。
